package ir.dotin.model.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by gh_abdoli on 4/3/16.
 */
public class ShortTerm extends Deposit implements Serializable {

    private final float INTEREST_RATE = .10f;

    public ShortTerm() {
    }

    public ShortTerm(long customerNumber, BigDecimal depositeBalance, int durationInDays, double payedInterest) {
        super(customerNumber, depositeBalance, durationInDays, payedInterest);
    }

    public float getInterestRate() {
        return INTEREST_RATE;
    }
}
