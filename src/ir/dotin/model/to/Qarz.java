package ir.dotin.model.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by gh_abdoli on 4/3/16.
 */
public class Qarz extends Deposit implements Serializable{

    private static final float INTEREST_RATE = 0;

    private Qarz(long customerNumber, BigDecimal depositeBalance, int durationInDays) {
        super(customerNumber, depositeBalance, durationInDays, 0);
    }

    public Qarz() {
    }

    public float getInterestRate() {
        return INTEREST_RATE;
    }
}
