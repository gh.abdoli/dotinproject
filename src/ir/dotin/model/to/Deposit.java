package ir.dotin.model.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by gh_abdoli on 4/3/16.
 */
public class Deposit implements Serializable{

    private long customerNumber;
    private BigDecimal depositeBalance;
    private int durationInDays;
    private double payedInterest;

    public Deposit() {
    }

    public Deposit(long customerNumber, BigDecimal depositeBalance, int durationInDays, double payedInterest) {
        this.customerNumber = customerNumber;
        this.depositeBalance = depositeBalance;
        this.durationInDays = durationInDays;
        this.payedInterest = payedInterest;
    }

    public double getPayedInterest() {
        return payedInterest;
    }

    public void setPayedInterest(double payedInterest) {
        this.payedInterest = payedInterest;
    }

    public long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public BigDecimal getDepositeBalance() {
        return depositeBalance;
    }

    public void setDepositeBalance(BigDecimal depositeBalance) {
        this.depositeBalance = depositeBalance;
    }

    public int getDurationInDays() {
        return durationInDays;
    }

    public void setDurationInDays(int durationInDays) {
        this.durationInDays = durationInDays;
    }
}
