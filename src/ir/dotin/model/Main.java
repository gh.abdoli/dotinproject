package ir.dotin.model;

import ir.dotin.model.bl.DepositManagerImpl;
import ir.dotin.model.to.Deposit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gh_abdoli on 4/4/16.
 */
public class Main {
    public static void main(String[] args) {
        DepositManagerImpl depositManager = new DepositManagerImpl();
        try {
            depositManager.readFromXML("deposits.xml");
            //depositManager.saveInFile("a.txt");
           // depositManager.sorting(new ArrayList<Deposit>());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
