package ir.dotin.model.bl;

import ir.dotin.model.to.Deposit;
import java.util.List;

/**
 * Created by gh_abdoli on 4/3/16.
 */
public interface DepositManager {
    public void readFromXML(String fileAddress) throws Exception;
}
