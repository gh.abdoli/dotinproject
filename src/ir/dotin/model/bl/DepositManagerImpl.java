package ir.dotin.model.bl;

import ir.dotin.model.da.DepositDA;
import ir.dotin.model.to.Deposit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gh_abdoli on 4/4/16.
 */
public class DepositManagerImpl implements DepositManager{
    @Override
    public void readFromXML(String fileAddress) throws Exception {
        DepositDA depositDA = new DepositDA();
        depositDA.readFromXML(fileAddress);

    }

}
