package ir.dotin.model.da;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import ir.dotin.model.to.Deposit;
import ir.dotin.model.to.Qarz;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 * Created by gh_abdoli on 4/3/16.
 */
public class DepositDA  {

    private static List list = new ArrayList();

    public void readFromXML(String fileAddress) throws Exception {
        File inputFile = new File(fileAddress);
        RandomAccessFile file = new RandomAccessFile("output.txt", "rw");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("deposit");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                Object object =  Class.forName("ir.dotin.model.to."
                        + eElement
                        .getElementsByTagName("depositType")
                        .item(0)
                        .getTextContent()).newInstance();

                Method customerNumberField = object.getClass().getMethod("setCustomerNumber", long.class);
                long customerNumber = Long.parseLong(eElement.getElementsByTagName("customerNumber")
                        .item(0)
                        .getTextContent());
                customerNumberField.invoke(object, customerNumber);

                Method depositBalanceField = object.getClass().getMethod("setDepositeBalance", BigDecimal.class);
                BigDecimal depositBalance = new BigDecimal(eElement
                        .getElementsByTagName("depositBalance")
                        .item(0)
                        .getTextContent());
                depositBalanceField.invoke(object,depositBalance);

                Method durationInDaysField = object.getClass().getMethod("setDurationInDays", int.class);
                int durationInDays = Integer.parseInt(eElement
                        .getElementsByTagName("durationInDays")
                        .item(0)
                        .getTextContent());
                durationInDaysField.invoke(object, durationInDays);

                calculatePayedInterest(object, depositBalance, durationInDays);

                saveInFile(file, object);

            }

        }
        file.close();

    }

    private void calculatePayedInterest(Object object, BigDecimal depositBalance, int durationInDays) throws Exception {

        Method setPayedInterest = object.getClass().getMethod("setPayedInterest", double.class);
        setPayedInterest.invoke(object, depositBalance
                .multiply(new BigDecimal(durationInDays))
                .multiply(new BigDecimal((float)
                        object.getClass().getMethod("getInterestRate")
                                .invoke(object)))
                .divide(new BigDecimal(36500), RoundingMode.HALF_UP).doubleValue());

        list.add(object);

    }

    private void saveInFile(RandomAccessFile file, Object object) throws Exception {

        StringBuilder str = new StringBuilder();
        str.append(object.getClass().getMethod("getCustomerNumber").invoke(object) +
                "#" + object.getClass().getMethod("getPayedInterest").invoke(object) + "\n");
        //System.out.println(str);
        file.write(str.toString().getBytes());
    }

}
